//
// C++ Interface: sitename
//
// Description:
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef SITENAME_H
#define SITENAME_H

class SiteName {
public:
	static constexpr char sitename[] = "github.com/tsailer/vfrnav-public";
	static constexpr char siteurlnoproto[] = "github.com/tsailer/vfrnav-public";
	static constexpr char siteurl[] = "http://github.com/tsailer/vfrnav-public";
	static constexpr char sitesecureurl[] = "https://github.com/tsailer/vfrnav-public";
};

#endif /* SITENAME_H */
