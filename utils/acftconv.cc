//
// C++ Implementation: acftconv
//
// Description: Aircraft File Format Conversion
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2007, 2009, 2013, 2014, 2015, 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "sysdeps.h"

#include <libxml++/libxml++.h>
#include <getopt.h>
#include <iomanip>
#include <iostream>
#include <fstream>

#ifdef HAVE_LIBARCHIVE
#include <archive.h>
#include <archive_entry.h>
#endif

#ifdef HAVE_JSONCPP
#include <json/json.h>
#endif

#ifdef HAVE_SYSEXITS_H
#include <sysexits.h>
#else
#define EX_USAGE   64
#define EX_DATAERR 65
#define EX_OK      0
#endif

#include "sitename.h"
#include "aircraft.h"

static void cout_deleter(std::ostream *ptr)
{
}

static void ostream_deleter(std::ostream *ptr)
{
	delete ptr;
}

static bool readbufferjson(Aircraft& acft, const std::string& buffer)
{
#ifdef HAVE_JSONCPP
	Json::Value root;
	{
		Json::Reader reader;
		if (!reader.parse(buffer, root, false))
			return false;
	}
	if (!acft.load_json(root))
		return false;
	return true;
#else
	return false;
#endif
}

static bool readbufferxml(Aircraft& acft, const std::string& buffer)
{
	xmlpp::DomParser p;
	p.parse_memory(buffer);
	if (!p)
		return false;
	xmlpp::Document *doc(p.get_document());
	xmlpp::Element *root(doc->get_root_node());
	if (root->get_name() != "aircraft")
		return false;
	acft.load_xml(root);
	return true;
}

static bool readzip(Aircraft& acft, const char *filename)
{
#ifdef HAVE_LIBARCHIVE
	if (!filename)
		return false;
	struct archive *a(archive_read_new());
	archive_read_support_format_zip(a);
	if (false)
		archive_read_support_format_all(a);
	int r(archive_read_open_filename(a, filename, 16384));
	if (r != ARCHIVE_OK)
		return false;
	struct archive_entry *entry(0);
	while (archive_read_next_header(a, &entry) == ARCHIVE_OK) {
		if (!strcmp(archive_entry_pathname(entry), "content.json")) {
			Json::Value root;
			{
				std::string buf;
				for (;;) {
					char buffer[8192];
					la_ssize_t size(archive_read_data(a, buffer, sizeof(buffer)));
					if (size < 0) {
						buf.clear();
						break;
					}
					if (size == 0)
						break;
					buf += std::string(buffer, size);
				}
				if (buf.empty()) {
					archive_read_data_skip(a);
					continue;
				}
				Json::Reader reader;
				if (!reader.parse(buf, root, false))
					continue;
			}
			if (!acft.load_json(root))
				continue;
			archive_read_free(a);
			return true;
		}
		archive_read_data_skip(a);
	}
	archive_read_free(a);
#endif
	return false;
}

static bool readjson(Aircraft& acft, const char *filename)
{
#ifdef HAVE_JSONCPP
	Json::Value root;
	{
		std::ifstream ifs(filename);
		Json::Reader reader;
		if (!reader.parse(ifs, root, false))
			return false;
	}
	if (!acft.load_json(root))
		return false;
	return true;
#else
	return false;
#endif
}

static bool readxml(Aircraft& acft, const char *filename)
{
	xmlpp::DomParser p(filename);
	if (!p)
		return false;
	xmlpp::Document *doc(p.get_document());
	xmlpp::Element *root(doc->get_root_node());
	if (root->get_name() != "aircraft")
		return false;
	acft.load_xml(root);
	return true;
}

#ifdef HAVE_LIBARCHIVE

static int archive_ostream_open_callback(struct archive *, void *client_data)
{
	if (!client_data)
		return ARCHIVE_FATAL;
	return ARCHIVE_OK;
}

static la_ssize_t archive_ostream_write_callback(struct archive *a, void *client_data, const void *buffer, size_t length)
{
	if (!client_data) {
		archive_set_error(a, EIO, "invalid output stream");
		return -1;
	}
	static_cast<std::ostream *>(client_data)->write(static_cast<const char *>(buffer), length);
	return length;
}

static int archive_ostream_close_callback(struct archive *, void *client_data)
{
	return ARCHIVE_OK;
}

#endif

int main(int argc, char *argv[])
{
	typedef enum {
		fmt_native,
		fmt_garminpilot,
		fmt_garminpilotjson
	} fmt_t;

	static struct option long_options[] = {
		{ "beautify", no_argument, 0, 0x100 },
		{ "fmt-native", no_argument, 0, 0x400 + fmt_native },
#ifdef HAVE_LIBARCHIVE
		{ "fmt-garminpilot", no_argument, 0, 0x400 + fmt_garminpilot },
#endif
		{ "fmt-garminpilotjson", no_argument, 0, 0x400 + fmt_garminpilotjson },
                 { 0, 0, 0, 0 }
	};
	int c, err(0);
	fmt_t ofmt(fmt_native);
	bool beautify(false);

	while ((c = getopt_long(argc, argv, "", long_options, 0)) != EOF) {
                switch (c) {
		case 0x100:
			beautify = true;
			break;

		case 0x400 + fmt_native:
		case 0x400 + fmt_garminpilot:
		case 0x400 + fmt_garminpilotjson:
			ofmt = (fmt_t)(c - 0x400);
			break;

		default:
			++err;
			break;
                }
        }
        if (err) {
                std::cerr << "usage: vfrnavacftconv [--fmt-native] [--fmt-garminpilot] [<infile>] [<outfile>]" << std::endl;
                return EX_USAGE;
        }
	Aircraft acft;
	try {
		if (optind < argc) {
			if (!readzip(acft, argv[optind]) &&
			    !readxml(acft, argv[optind]) &&
			    !readjson(acft, argv[optind])) {
				std::cerr << "Cannot read input file \"" << argv[optind] << "\"" << std::endl;
				return EX_DATAERR;
			}
			++optind;
		} else {
			std::istreambuf_iterator<char> eos;
			std::string buf(std::istreambuf_iterator<char>(std::cin), eos);
			if (!readbufferxml(acft, buf) &&
			    !readbufferjson(acft, buf)) {
				std::cerr << "Invalid input from stdin" << std::endl;
				return EX_DATAERR;
			}
		}
	} catch (const std::exception& ex) {
		std::cerr << "exception: " << ex.what() << std::endl;
	}
	{
		Aircraft::recompute_t r(acft.recompute());
		if (r & Aircraft::recompute_wbunits)
			std::cerr << "Aircraft " << acft.get_callsign() << " W&B auto units added" << std::endl;
		if (r & Aircraft::recompute_distances)
			std::cerr << "Aircraft " << acft.get_callsign() << " distance polynomials recalculated" << std::endl;
		if (r & Aircraft::recompute_climb)
			std::cerr << "Aircraft " << acft.get_callsign() << " climb polynomials recalculated" << std::endl;
		if (r & Aircraft::recompute_descent)
			std::cerr << "Aircraft " << acft.get_callsign() << " descent polynomials recalculated" << std::endl;
		if (r & Aircraft::recompute_glide)
			std::cerr << "Aircraft " << acft.get_callsign() << " glide polynomials recalculated" << std::endl;
	}
	switch (ofmt) {
	case fmt_native:
		if (optind < argc)
			acft.save_file(argv[optind]);
		else
			std::cout << acft.save_string();
		break;

#ifdef HAVE_LIBARCHIVE
	case fmt_garminpilot:
	{
		typedef std::unique_ptr<std::ostream, void (*)(std::ostream *)> os_t;
		os_t os(&std::cout, cout_deleter);
		if (optind < argc)
			os = os_t(new std::ofstream(argv[optind]), ostream_deleter);
		struct archive *a(archive_write_new());
		archive_write_add_filter_gzip(a);
		archive_write_set_format_ustar(a);
		archive_write_set_filter_option(a, "gzip", "compression-level", "9");
		archive_write_set_bytes_per_block(a, 0);
		archive_write_open(a, os.get(), archive_ostream_open_callback,
				   archive_ostream_write_callback,
				   archive_ostream_close_callback);
		try {
			std::string contents;
			if (beautify) {
				Json::Value root;
				acft.save_garminpilot(root);
				Json::StyledWriter writer;
				contents = writer.write(root);
			} else {
				Json::Value root;
				acft.save_garminpilot(root);
				Json::FastWriter writer;
				contents = writer.write(root);
			}
			struct archive_entry *entry = archive_entry_new();
			archive_entry_set_pathname(entry, "content.json");
			archive_entry_set_size(entry, contents.size());
			archive_entry_set_filetype(entry, AE_IFREG);
			archive_entry_set_perm(entry, 0666);
			archive_entry_set_uname(entry, "mobile");
			archive_entry_set_gname(entry, "mobile");
			{
				time_t tm(time(0));
				archive_entry_set_atime(entry, tm, 0);
				archive_entry_set_birthtime(entry, tm, 0);
				archive_entry_set_ctime(entry, tm, 0);
				archive_entry_set_mtime(entry, tm, 0);
			}
			archive_write_header(a, entry);
			archive_write_data(a, contents.c_str(), contents.size());
			archive_entry_free(entry);
		} catch (...) {
			archive_write_close(a);
			archive_write_free(a);
			throw;
		}
		archive_write_close(a);
		archive_write_free(a);
		break;
	}
#endif

	case fmt_garminpilotjson:
	{
		typedef std::unique_ptr<std::ostream, void (*)(std::ostream *)> os_t;
		os_t os(&std::cout, cout_deleter);
		if (optind < argc)
			os = os_t(new std::ofstream(argv[optind]), ostream_deleter);
		if (beautify) {
			Json::Value root;
			acft.save_garminpilot(root);
			Json::StyledWriter writer;
			*os << writer.write(root);
		} else {
			Json::Value root;
			acft.save_garminpilot(root);
			Json::FastWriter writer;
			*os << writer.write(root);
		}
		break;
	}

	default:
		return EX_USAGE;
	}
	return EX_OK;
}
